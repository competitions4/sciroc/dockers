# SciRoc Competition 2021 - Technical environment

Welcome to the competition. In this repository you should find all the technical details to develop your solution for the competition.

## Deliverable guidelines

The deliverable solution of episode 1 and 2 of sciroc will consist of one single file, a docker image.

This docker image will have a catkin workspace inside `$HOME/ws/` containing a single launchfile that can be called using `roslaunch sciroc_episode_1 solution.launch`. This launchfile should start the simulation with the solution.

### Suggested workflow to work with dockers

Let's define the probably most common scenario where there is a group team member composed by 1 team leader and multiple software developers.

#### Create a private team gitlab repository
You create a private gitlab.com repository where all the team has write access, lets call it `private_gitlab`
In this repository you will have:
- A docker image (your docker image solution build on top of the [competition image](https://gitlab.com/competitions4/sciroc/dockers/container_registry/1981507))
- A docker file (with all the steps to create your docker image)
- A workspace folder (with all the files and ros packages you created)

[Find here a real example](https://gitlab.com/daniel.lopez.puig/competition-team-example)

#### Update your docker image
There are two main reasons you should update the docker image of the gitlab repository:

1) The competition docker image has changed (you would be notified via email)
2) A team member finished a package or modified the Dockerfile and need to be tested and used by other members

To do so the process should be the following:
```bash
#Download the latest competition docker image, note you have to substitute <X.X>
docker login registry.gitlab.com
docker pull registry.gitlab.com/competitions4/sciroc/dockers/sciroc:<X.X>
# Update your local private_gitlab containing the latest Dockerfile
cd <private_gitlab_path>
git pull --rebase
# Build your image from competition
docker build .
# Tag the pulled docker with your personal docker name
docker tag registry.gitlab.com/competitions4/sciroc/dockers/sciroc:<X.X> <docker_remote_path_name>
# Upload your new image to private_gitlab
docker push <docker_remote_path_name>
```

#### Daily developer workflow
**It is very important that any significant change you do inside the docker is added to the Dockerfile!**

Generally you will need to install external ros packages, libraries or programs in your docker.
Every time do it and verify that is required, you should add the line to the Dockerfile.

The tipicall use case is:

`sudo apt install ros-melodic-<pkg-name>`

So you should add in the docker file the following:

`RUN apt install -y ros-melodic-<pkg_name>`

Check the [Dockerfile provided as example in this repo](https://gitlab.com/competitions4/sciroc/dockers/-/blob/master/Dockerfile)


Regarding your daily development, you should place your files inside the catkin shared workspace of the docker.
We suggest to create a `$HOME/ws/src` working directory in your PC and then share it with the docker:

- Tell to your [Dockerfile](https://gitlab.com/competitions4/sciroc/dockers/-/blob/master/Dockerfile#L5-10) your working directory, this way when you build the image it will copy and install the packages in the docker.

- Start the docker creating a shared folder `-v /$HOME/ws/src:/home/user/ws` with your host PC, saving always the changes in your PC.

```bash
~/pal_docker_utils/scripts/pal_docker.sh -it -v /dev/snd:/dev/snd -v /$HOME/ws/src:/home/user/ws registry.gitlab.com/private_gitlab
```

### Real robot deployment

**Participants that bring their own robot do not need to follow the following steps as they will not share the robot with other participants**

In the final stage at Bologna, a single docker image file will be placed inside the TIAGo. After that, a single launchfile should start all the required nodes to solve the solution.

The main reason we require this is because we will ensure no compatibility issues will rise when moving from simulation to real robot and the changes in the environment will not affect the following participants that deploy to the same robot.

The easiest way to test your docker solution will work in the real robot is to run two dockers in the same pc:
- The docker of the competition launching the simulation `roslaunch tiago_998_gazebo tiago_navigation.launch`
- Your docker with the solution `roslaunch sciroc_episode_1 solution.launch`

Remember that [text-to-speech and text-display can only be tested in the real robot](https://gitlab.com/competitions4/sciroc/dockers#real-robot-text-to-speech-and-text-to-sceen)

## Docker

This repository contains one docker image named `registry.gitlab.com/competitions4/sciroc/dockers/sciroc`

### What is a docker?

Docker is a platform for developers and sysadmins to develop, deploy, and run applications with containers. The use of Linux containers to deploy applications is called containerization. Containers are not new, but their use for easily deploying applications is.

Containerization is increasingly popular because containers are:

- Flexible: Even the most complex applications can be containerized.
- Lightweight: Containers leverage and share the host kernel.
- Interchangeable: You can deploy updates and upgrades on-the-fly.
- Portable: You can build locally, deploy to the cloud, and run anywhere.
- Scalable: You can increase and automatically distribute container replicas.
- Stackable: You can stack services vertically and on-the-fly.

If you are interested on how docker works, visit her [webpage](https://docs.docker.com/)

### Where is the docker image located?

On gitlab.com, your image should be inside the *Container Registry* of this project. This is accessible on the left sidebar, under the *Packages & Registries* entry.

You can use the **copy** button to retrieve the url for using it, it will be needed later instead of PATH\_TO\_YOUR\_DOCKER\_IMAGE

### Run the docker

In order to start the docker loaded on this repository, you will need to log in the docker daemon on gitlab.com:

```
$ docker login registry.gitlab.com
```
an then enter your personal GitLab user and password.

For your convenience, we have published some scripts that simplify the launch of a docker with GPU acceleration. Follow the instructions at [pal_docker_utils](https://github.com/pal-robotics/pal_docker_utils/) to properly set up your environment with nvidia-docker. If you do not follow the steps properly, you will not be able to run gazebo, rviz or other graphical applications from within the docker container.

Once logged and after configuring pal\_docker\_utils, you will need to execute the [pal_docker.sh](https://github.com/pal-robotics/pal_docker_utils/blob/master/scripts/pal_docker.sh) script with the name of the image and the application you want to start.
```
$ ./pal_docker.sh -it -v /dev/snd:/dev/snd PATH_TO_YOUR_DOCKER_IMAGE
```
The previous command starts a bash terminal inside a container of the specified image.

Example for this repo:
```
./pal_docker.sh -it -v /dev/snd:/dev/snd registry.gitlab.com/competitions4/sciroc/dockers/sciroc
```

**Attention!** Remember that once inside the docker, any file created or any program installed on the docker will be deleted, if the specific file was not saved on the exchange folder. If the user needs to install a speicifc software everytime, it is better to create a new docker following the instructions on the [tutorials](https://docs.docker.com/get-started/), and taking as a base the desired docker.
 image.

---

## Start the simulation
```bash
source /opt/pal/ferrum/setup.bash
roslaunch tiago_998_gazebo tiago_navigation.launch
```
## Built in functionalities
### Navigation
List current points of interest (POIs):
```bash
rosparam get /mmap/poi/submap_0/ | grep table
```
![list of pois](media/list_pois.png)

Now we can simply go to the desired poi using the `go_to_poi` action specifying the name
```bash
rostopic pub /poi_navigation_server/go_to_poi/goal pal_navigation_msgs/GoToPOIActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  poi:
    data: 'table_1'" 
```

To know in what zone of interest you are in:

`rostopic echo -n1 /current_zone_of_interest`

### HRI
#### Play motions / move joints
1) Move one joint using play_motion
`rosrun play_motion move_joint <joint_name> <final_position> <allocated_time>`
Example:
`rosrun play_motion move_joint arm_2_joint 0.0 2.0`

You can also run an entire recorded motion:
`rosparam list | grep play_motion`
`rosrun play_motion run_motion wave`

2) Move each joint using rqt_joint_trajectory_controller:
`rosrun rqt_joint_trajectory_controller rqt_joint_trajectory_controller`

#### Record motions
1) Using play_motion_builder (recommended way)
Open 2 terminals:
T1) 
`rosrun play_motion_builder play_motion_builder_node`

T2)
`rosrun rqt_play_motion_builder rqt_play_motion_builder`

Find more information [in ros wiki](http://wiki.ros.org/play_motion_builder)

2) Manually create a yaml file:
Copy & paste an already yaml file and do the modifications.
Find some examples in `/opt/pal/ferrum/share/tiago_bringup/config/motions/`


**Remember to load the yaml files created in method 1) or 2)**
```bash
rosparam load <yaml_file_path>
```

#### Real robot Text to Speech and Text to Sceen
##### Text to Screen
Text sent to topic `/web_text_display` will be displayed in the web server http://tiago-\<sn\>c:11011

Display text:

`rostopic pub -1 /web_text_display std_msgs/String "data: 'Hi, my name is TIAGo!'"`

Clear page:

`rostopic pub -1 /web_text_display std_msgs/String "data: '#CLEAR'"`

New line with beautiful style:

`rostopic pub -1 /web_text_display std_msgs/String "data: '#NEW_LINE'"`



**If you have a robot and want to test the screen display, open a ticket to our support platform and we will add to it**

##### Text to Speech
Publish the text that you want the robot to say:
```bash
rostopic  pub /tts/goal pal_interaction_msgs/TtsActionGoal "header: {}
goal_id: {}
goal:
  text: {}
  rawtext:
    text: 'Hello world'
    lang_id: 'en_GB'
  speakerName: ''
  wait_before_speaking: 0.0" --once
```
#### Simulate rbg and microphone from your hardware pc
To ensure that the docker has access to your hardware you can use the following command to test it.

```bash
arecord -l
#take note of card and device number to put instead of hw:0,0
# record some noise
arecord -f cd -d 5 -D hw:0,0 /tmp/micro.wav
# check if you can reproduce it
aplay -D hw:0,0 /tmp/micro.wav
```

When you run the simulation, you will be able access the camera of your pc if it has been properly configured:

Camera:

launch and listen manually the default values:

```bash
rosrun usb_cam usb_cam-test.launch
rostopic echo /usb_cam/image_raw
```
Microphone:

launch manually:
```bash
roslaunch audio_capture capture.launch channels:=2 sample_rate:=44100 device:=hw:1,0
```

In order to find this parameters run this command on your machine with pulseaudio installed:
```bash
pactl list short sinks
```

Finally run this command to check that the topic is published :
```bash
rostopic echo /audio/audio
```

In order to modify the parameters of previous packages:
`rosed tiago_998_gazebo sciroc_extras.launch`

In order to modify the parameters of the microphone (if format mp3 is not working you can try wave):
`rosed audio_capture capture.launch`

---

## Create a docker image with your packages and configurations
In this competition user should only provide a single docker image that should work out of the box with everything compiled, installed and sourced.
In order to achieve this and specially if you are not very experienced with dockers we are going to provide some tips.

### Create a docker image with your **compiled** packages
In this repository you can find the `Dockerfile`. There you have to add the commands to:
  1) Install debians (e.g. ros-melodic-<pkg_name>)
  2) Add your new/modified ros packages

After that you need to build the image:
```bash
docker build -t <your-name> .
```

Save the image:
```bash
docker save -o <output_full_path> <docker_image_tag>
```
We strongly suggest that you first test it in another machine to ensure everything works out of the box.
Load the previous image:
```bash
docker load < <docker_image_name>.tar.gz
docker images # check it is
```

Finally send to tiago-support@pal-robotics.com the docker image. Ensure it will work out of the box using [run a docker](#Run-a-docker) replacing `PATH_TO_YOUR_DOCKER_IMAGE` for your docker image.

## Run Sciroc EP 1 environment
In order to add the scene and the Ros services of Sciroc Episode 1, you need to add the `sciroc_ep1_object_manager` package to the ros workspace in the exchange folder. The package is available at [webpage](https://github.com/vincenzosu/Sciroc2EP1-SimulatedEnv)

To run the simulation and the needed nodes, you can execute:
``` bash
roslaunch tiago_998_gazebo tiago_navigation.launch
```

#### Interacting with the simulation
The objects provided for the counter are:
```  bash 
beer, cocacola, plastic_cup, bifrutas_tropical_can, biscuits_pack, sprite, pringles1, pringles2
```

The services to manipulate them are:
```  bash
rosservice call /sciroc_object_manager/get_three_ordered_items 
"item0: 'biscuits_pack' 
item1: 'plastic_cup' 
item2: 'cocacola"  
```
The service can be invoked only if the robot is less than 1.5 meter far from the counter.


```  bash 
rosservice call /sciroc_object_manager/change_the_item 
"name_of_the_object_to_change: 'biscuit' 
name_of_the_object_to_spawn: beer'"
```
The service return a boolean and string. When the boolean is false, the string contains the error description.


```  bash 
rosservice call /sciroc_object_manager/move_items_on_the_tray "{}"
```
This service does not require arguments and packs all the items on the counter and put them on the robot's tray.


```  bash 
rosservice call /sciroc_object_manager/move_items_on_the_closest_table "{}"
```
This service does not require arguments and move all the items on the items that are on the robot's tray on the closest table.

**NB: In the next releases of the container the package will be embedded in the docker container.**

